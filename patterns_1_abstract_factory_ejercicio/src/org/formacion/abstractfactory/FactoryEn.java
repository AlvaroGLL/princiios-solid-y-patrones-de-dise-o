package org.formacion.abstractfactory;

public class FactoryEn implements AbstractFactorySaludosPreguntas{

	@Override
	public Saludos createSaludos() {
		return new SaludosEn();
	}

	@Override
	public Preguntas createPreguntas() {
		return new PreguntasEn();
	}

}
