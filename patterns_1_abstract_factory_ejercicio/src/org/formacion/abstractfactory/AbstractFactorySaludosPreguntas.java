package org.formacion.abstractfactory;

public interface AbstractFactorySaludosPreguntas {

	Saludos createSaludos();
	Preguntas createPreguntas();
}
