package org.formacion.abstractfactory;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestMultiIdioma {

	@Test
	public void test_es() {
		Preguntas preguntas = new FactoryEs().createPreguntas();
		
		assertEquals("¿qué hora es?", preguntas.preguntaHora() );
		assertEquals("¿qué tiempo hace?", preguntas.preguntaTiempo() );
		
		Saludos saludos = new FactoryEs().createSaludos();
		
		assertEquals("buenos días", saludos.buenosDias());
		assertEquals("buenas tardes", saludos.buenasTardes());
	}
	
	@Test
	public void test_en() {
		Preguntas questions = new FactoryEn().createPreguntas();
		
		assertEquals("what time is it?", questions.preguntaHora() );
		assertEquals("how is the weather?", questions.preguntaTiempo() );
		
		Saludos greetings = new FactoryEn().createSaludos();
		
		assertEquals("good morning", greetings.buenosDias());
		assertEquals("good afternoon", greetings.buenasTardes());
	}

}
