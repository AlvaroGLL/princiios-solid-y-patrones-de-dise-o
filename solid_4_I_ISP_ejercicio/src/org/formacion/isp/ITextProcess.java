package org.formacion.isp;

public interface ITextProcess {
	
	public void nueva (String palabra);
	
	public String texto ();

}
