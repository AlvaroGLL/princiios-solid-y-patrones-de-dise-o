package org.formacion.isp;

public interface ICorrectText {
	
	public boolean correcto (Idioma idioma);

}
