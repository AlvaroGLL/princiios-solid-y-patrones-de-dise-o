package org.formacion;

public class InternationalMoneyOrganizationAdapter implements InternationalMoneyOrganization {
	
	private BancoNostrumImpl bancoNostrumImpl;
	
	public InternationalMoneyOrganizationAdapter(BancoNostrumImpl bancoNostrumImpl) {
		this.bancoNostrumImpl = bancoNostrumImpl;
	}

	@Override
	public void transfer(int cantidad, String cliente) {
		bancoNostrumImpl.movimiento(cliente, cantidad);
	}

	@Override
	public int state(String cliente) {
		try {
			return bancoNostrumImpl.estado(cliente);
		} catch (NullPointerException e) {
			return 0;
		
		}
	}

}
