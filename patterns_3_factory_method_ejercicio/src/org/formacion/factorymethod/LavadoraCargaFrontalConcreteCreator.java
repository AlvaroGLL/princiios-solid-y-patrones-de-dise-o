package org.formacion.factorymethod;

public class LavadoraCargaFrontalConcreteCreator extends LavadoraCreator{
	
	protected Lavadora creaLavadora() {
		return new LavadoraCargaFrontal();
	}

}
