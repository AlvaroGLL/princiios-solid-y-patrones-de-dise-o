package org.formacion.factorymethod;

public abstract class LavadoraCreator {
	public Lavadora crea() {
		Lavadora lavadora = creaLavadora();
		
		lavadora.ponerMandos();
		lavadora.ponerTambor();
		
		return lavadora;
	}
	
	protected abstract Lavadora creaLavadora();

}
