package org.formacion.factorymethod;

public class LavadoraCargaSuperiorConcreteCreator extends LavadoraCreator{

	protected Lavadora creaLavadora() {
		return new LavadoraCargaSuperior();
	}
}
