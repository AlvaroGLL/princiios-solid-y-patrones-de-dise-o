package org.formacion.flyweight;

import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		
		int numJugadores = 3;
		
		CamisetaFlyweight camiseta  = new CamisetaFlyweight();
		
		Set<Jugador> jugadores = new HashSet<Jugador>();
		
		for (int i = 1; i <= numJugadores; i++) {
			jugadores.add(new Jugador(camiseta, i));
		}
		
		jugadores.forEach(j -> j.dibuja());

	}

}
