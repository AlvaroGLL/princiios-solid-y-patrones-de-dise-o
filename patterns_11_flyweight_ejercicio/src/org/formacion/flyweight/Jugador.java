package org.formacion.flyweight;

public class Jugador {

	CamisetaFlyweight camiseta;
	int numero;
	
	public Jugador(CamisetaFlyweight camiseta, int numero) {
		this.camiseta = camiseta;
		this.numero = numero;
	}
	
	
 	public void dibuja() {
 		System.out.println(camiseta.getCamiseta(numero));
 	}

}
